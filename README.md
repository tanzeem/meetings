# ![Meetings Icon](static/meetings_logo.jpg) MEETINGS 

Plans and proceedings of meetings conducted by Free Software Users' Group, Trivandrum are
preserved here. They can be accessed from links below.

## IMPORTANT: Call For Participation
Free Software activists are asked for participating in the future activities 
with emphasis on building and selling RYF (Respect Your Freedom) devices.

## Next Meeting
```
Date   : Dec 31st, 2017
Time   : 3:00 PM to 6:00 PM
Venue  : Museum
Topics :
    1. Followup of previous meeting [Meeting on 24th Dec](proceedings/2017-12-24.md) [Planning Session]
```

## Previous Meetings

### December 2017

### August 2017
- [Meeting on 27th](proceedings/2017-08-27.md) [General Session]
- [Meeting on 20th](proceedings/2017-08-20.md) [Learning Session]
- [Meeting on 13th](proceedings/2017-08-13.md) [Learning Session]

## Credits
**Meetings Logo:** [Scott Maxell](https://thegoldguys.blogspot.in/).
Shared from [flickr.com](https://www.flickr.com/photos/lumaxart/2181400330/in/photostream/)
under [CC BY-SA 2.0 license](https://creativecommons.org/licenses/by-sa/2.0/)

**Group Hosting:** [Free Software Community of India](http://fsci.org.in/)
